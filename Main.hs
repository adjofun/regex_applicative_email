module Main where

import Control.Monad
import Data.Foldable
import Text.Regex.Applicative

import Data.Char
import Data.List

main :: IO ()
main = do
  print "MATCHES"
  forM_ matches $ \example -> putStrLn $ example ++ " =~ " ++ show (example =~ email) 
  putStrLn "\n===\n"
  print "NONMATCHES"
  forM_ nonmatches $ \example -> putStrLn $ example ++ " =~ " ++ show (example =~ email) 


matches :: [String]
matches = 
  [ "asmith@mactec.com"
  , "foo12@foo.edu"
  , "bob.smith@foo.tv"
  , "joe@123aspx.com"
  , "joe@web.info"
  , "joe@company.co.uk" 
  , "test@[192.186.0.1]"
  ]

nonmatches :: [String]
nonmatches =
  [ "joe"
  , "@foo.com"
  , "a@a"
  , "gs_.gs@com.com"
  , "gav@gav.c"
  , "jim@--c.ca"
  ]

-- https://tools.ietf.org/html/rfc2822
-- http://regexlib.com/Search.aspx?k=email
emailRegexp :: String
emailRegexp = 
  "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$"

data Email = Email {
  localPart :: String
, domain :: Domain
} deriving (Eq, Show)

data Domain =
   DNS String
 | IP String deriving (Eq, Show)

email :: RE Char Email
email = 
  Email <$> localPartP 
        <*  sym '@'
        <*> domainP

localPartP :: RE Char String
localPartP =
  some $ psym isLocalPartChar

domainP :: RE Char Domain
domainP =
      IP  <$> (sym '[' *> ip <* sym ']')
  <|> DNS <$> dnspart `sepBy` (sym '.')

ip :: RE Char String
ip =
  let ippart = boundedQty 1 3 (psym isDigit)
  in sep ippart 4 (sym '.')

dnspart :: RE Char String
dnspart = boundedQty 1 63 (psym isDNSChar)

-- Helpers --
qty :: Int -> RE s a -> RE s [a]
qty = replicateM

boundedQty :: Int -> Int -> RE s a -> RE s [a]
boundedQty low high regex = 
  foldr (<|>) empty $ ($regex) <$> qty <$> [low..high]

sep :: RE s [a] -> Int -> RE s a -> RE s [a]
sep regex times separatorRegex = 
  join <$> (sequenceA $ intersperse (singleton separatorRegex) (replicate times regex))

sepBy1 :: RE s a -> RE s a -> RE s [a]
sepBy1 p s = scan where scan = liftA2 (:) p (((:) <$> s <*> scan) <|> pure [])

sepBy :: RE s [a] -> RE s a -> RE s [a]
sepBy p s = join <$> scan where scan = liftA2 (:) p (((:) <$> (singleton s) <*> scan) <|> pure [])

singleton :: RE s a -> RE s [a]
singleton = fmap (:[]) 

isLocalPartChar :: Char -> Bool
isLocalPartChar c = 
  isDigit c || isAsciiUpper c || isAsciiLower c || c == '.'

isDNSChar :: Char -> Bool
isDNSChar c =
  isDigit c || isAsciiUpper c || isAsciiLower c || c == '-'
